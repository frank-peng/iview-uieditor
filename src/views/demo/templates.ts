
export const DeomTemplates = [
  {
    id: 'start',
    name: '开始使用',
    template: `<template>
  <iview-uieditor
    :options="options"
    :json="json"
    :theme="theme"
  />
</template>
<script>
export default {
  data() {
    return {
      options: UERender.DefineOption({}),
      theme: {},
      json: {
        type: "Card",
        props: {
          class: "mb-sm",
          title: "Card 卡片",
        },
        children: [
          {
            type: "uieditor-text",
            props: {
              text: "文本内容",
            },
          },
        ],
      }
    };
  },
};
</script>
`
  },
  {
    id: 'render',
    name: '使用渲染组件渲染内容',
    template: `<template>
    <uieditor-div style="width:100%;height:100%">
      <iview-uieditor-render :options="options" :json="jsonRender"></iview-uieditor-render>
      <iview-uieditor :options="options" :json="json" :theme="theme"></iview-uieditor>
    </uieditor-div>
  </template>
  <script>
    export default {
    data() {
      return {
        options: UERender.DefineOption({}),
        theme: {
          toolBar: [
            {
              title: "应用",
              click: ({ service }) => {
                this.jsonRender = service.getJson();
              },
            }
          ]
        },
        jsonRender:{
          type:'uieditor-text',
          props:{
            text:'请使用用编辑器中的“应用”功能在这里渲染编辑内容'
          }
        },
        json: {
          type: "Card",
          props: {
            class: "mb-sm",
            title: "Card 卡片(test)",
          },
          children: [
            {
              type: "uieditor-text",
              props: {
                text: "TEST",
              },
            },
          ],
        }
      };
    },
  };
  </script>`
  },
  {
    id: 'setup-start',
    name: 'Vue 3.x 组合式 API Setup',
    template: `<template>
  <iview-uieditor
    :options="options"
    :json="json"
    :theme="theme"
  />
</template>
<script>
export default {
  data() {
    return {
      options: UERender.DefineOption({}),
      theme: {},
      json: {
        "type": "uieditor-div",
        "children": [
          {
            "type": "script",
            "children": [
              \`{\n  setup(props, context) {\n    const { reactive, onBeforeMount } = VueCompositionApi;\n    const _data = reactive({\n      text: 'now: '\n    });\n\n    onBeforeMount(function () {\n      setTimeout(function () {\n        _data.text += new Date().valueOf();\n      }, 1000);\n    });\n\n    return {\n      data: _data\n    }\n  }\n}\`
            ]
          },
          {
            "type": "Card",
            "props": {
              "class": "mb-sm",
              "title": "Card 卡片"
            },
            "children": [
              {
                "type": "uieditor-text",
                "props": {
                  ":text": "$this.data.text"
                }
              }
            ]
          }
        ]
      }
    };
  },
};
</script>
`
  },
  {
    id: 'toolbar',
    name: '定义工具栏',
    template: `<template>
    <uieditor-div style="width:100%;height:100%">
      <Input type="textarea" :rows="4" v-model="text">
          </Input>
      <iview-uieditor :options="options" :json="json" :theme="theme" />
    </uieditor-div>
  </template>
  <script>
    export default {
          data() {
            return {
              options: UERender.DefineOption({}),
              theme: {
                toolBar: [
                  {
                    title: "获取JSON",
                    click: ({ service }) => {
                      this.text = _.trim(service.getTmpl());
                    },
                  },
                  {
                    title: "获取JSON icon",
                    icon:'ivu-icon ivu-icon-md-checkbox-outline',
                    click: ({ service }) => {
                      this.text = JSON.stringify(service.getJson());
                    },
                  }
                ]
              },
              text:'',
              json: {
                type: "Card",
                props: {
                  class: "mb-sm",
                  title: "Card 卡片(test)",
                },
                children: [
                  {
                    type: "uieditor-text",
                    props: {
                      text: "TEST",
                    },
                  },
                ],
              }
            };
          },
        };
  </script>`
  },
  {
    id: 'options_mixins',
    name: 'options:mixins and global 组合Vue组件与设置全局属性与方法',
    template: `<template>
  <iview-uieditor :options="options" :json="json" :theme="theme" />
</template>
<script>
export default {
  data() {
    return {
      options: UERender.DefineOption({
        global() {
          return {
            merge(text) {
              return \`$\{text} - merge\`;
            },
          };
        },
        mixins: [
          {
            data() {
              return {
                title: "hello",
              };
            },
            created() {},
          },
        ],
      }),
      theme: {},
      json: {
        type: "Card",
        props: {
          class: "mb-sm",
          ":title": "\`Card 卡片 - $\{merge(title)}\`",
        },
        children: [
          {
            type: "uieditor-text",
            props: {
              text: "文本内容",
            },
          },
        ],
      },
    };
  },
};
</script>`
  },
  {
    id: 'options_templates',
    name: 'options:templates 设置模板',
    template: `<template>
  <iview-uieditor :options="options" :json="json" :theme="theme" />
</template>
<script>
export default {
  data() {
    return {
      options: UERender.DefineOption({
        templates: [
          {
            title: "JSON Object",
            group: "测试模板库/测试模板",
            json: {
              type: "Input",
            },
          },
          {
            title: "Tmpl",
            group: "测试模板库/测试模板",
            template: \`<Input />\`,
          },
        ],
      }),
      theme: {},
      json: {
        type: "Card",
        props: {
          class: "mb-sm",
          "title": "Card 卡片",
        },
        children: [
          {
            type: "uieditor-text",
            props: {
              text: "文本内容",
            },
          },
        ],
      },
    };
  },
};
</script>`
  },
  {
    id: 'options_http',
    name: 'options:http 数据源',
    template: `<template>
    <iview-uieditor :options="options" :json="json" :theme="theme" />
  </template>
  <script>
    export default {
    data() {
      return {
        options: UERender.DefineOption({
          //要使用数据源请配置http
          http() {
            return {
              async get(url, config) {
                return { url, name: "name-" + UEHelper.makeAutoId(), config };
              },
              async post(url, config) {
                return { url, name: "name-" + UEHelper.makeAutoId(), config };
              },
            };
          },
        }),
        theme: {},
        json: {
          "type": "uieditor-div",
          "children": [
            {
              "type": "uieditor-div",
              "children": [
                {
                  "type": "uieditor-text",
                  "props": {
                    "text": "演示数据源"
                  }
                }
              ]
            },
            {
              "type": "uieditor-div",
              "children": [
                {
                  "type": "uieditor-text",
                  "props": {
                    "datasource-name": "test",
                    "datasource-url": "url111",
                    ":datasource-data": "{data1:1111}",
                    ":datasource-query": "{query2:2222}",
                    "datasource-method": "get",
                    ":datasource-auto": "true",
                    ":text": "\`请求结果 - \${JSON.stringify($this.$ds.test)}\`"
                  }
                }
              ]
            },
            {
              "type": "uieditor-div",
              "children": [
                {
                  "children": [
                    {
                      "type": "uieditor-text",
                      "props": {
                        "text": "刷新"
                      }
                    }
                  ],
                  "type": "uieditor-a",
                  "props": {
                    "ue-is-lock": true,
                    "@click": "$this.$dsRefs.test?.send()"
                  }
                }
              ]
            }
          ]
        },
      };
    },
  };
  </script>`
  },
  {
    id: 'options_transfer',
    name: 'options:transfer 定义可编辑组件（普通容器，如：div）',
    template: `<template>
  <iview-uieditor :options="options" :json="json" :theme="theme" />
</template>
<script>
  export default {
  data() {
    return {
      options: UERender.DefineOption({
        transfer:UERender.DefineTransfer({
        'demo-div': {
          type:'div',
          "editor": {
            text: 'Div 块级标签',
            order: 0,
            groupOrder:10,
            group:'Demo组件库/基础组件',
            icon: 'layui-icon layui-icon-template-1',
            container: true
          }
        }
      }),
      }),
      theme: {},
      json: {
        type: "Card",
        props: {
          class: "mb-sm",
          "title": "Card 卡片",
        },
        children: [
          {
            type: "uieditor-text",
            props: {
              text: "文本内容",
            },
          },
        ],
      },
    };
  },
};
</script>`
  },
  {
    id: 'options_transfer_base',
    name: 'options:transfer 定义可编辑组件（普通组件，如：text）及设置属性与事件',
    template: `<template>
  <iview-uieditor :options="options" :json="json" :theme="theme" />
</template>
<script>
  export default {
    data() {
      return {
        options: UERender.DefineOption({
          transfer:UERender.DefineTransfer({
            'demo-text': {
              type:'uieditor-text',
              "editor": {
                text: 'Text 文本',
                order: 4,
                groupOrder:10,
                group:'Demo组件库/基础组件',
                inline: true,
                icon: 'layui-icon layui-icon-align-left',
                attrs: {
                  text: {
                    effect: true,
                    value: "文本内容",
                    order: 0
                  },
                  click: { event: true }
                }
              }
            }
        }),
        }),
        theme: {},
        json: {
          type: "Card",
          props: {
            class: "mb-sm",
            "title": "Card 卡片",
          },
          children: [
            {
              type: "uieditor-text",
              props: {
                text: "文本内容",
              },
            },
          ],
        },
      };
    },
  };
</script>`
  },
  {
    id: 'options_transfer_cxtmenu',
    name: 'options:transfer 定义可编辑组件快捷菜单（右击菜单）',
    template: `<template>
  <iview-uieditor :options="options" :json="json" :theme="theme" />
</template>
<script>
  export default {
  data() {
    return {
      options: UERender.DefineOption({
        transfer:UERender.DefineTransfer({
        'demo-div': {
          type:'div',
          "editor": {
            text: 'Div 块级标签',
            order: 0,
            groupOrder:10,
            group:'Demo组件库/基础组件',
            icon: 'layui-icon layui-icon-template-1',
            container: true,
            contextmenu({ render, service }) {
              return [
                {
                  title: "添加 Text",
                  click() {
                    service.addByComponent(
                      {
                        $isTmpl: true,
                        item: {
                          json:
                            '{"type":"uieditor-text","props":{"text":"test1222"}}',
                        },
                      },
                      render.editorId,
                      "in"
                    );
                  },
                },
              ];
            }
          }
        }
      }),
      }),
      theme: {},
      json: {
        type: "Card",
        props: {
          class: "mb-sm",
          "title": "Card 卡片",
        },
        children: [
          {
            type: "uieditor-text",
            props: {
              text: "文本内容",
            },
          },
        ],
      },
    };
  },
};
</script>`
  },
  {
    id: 'options_transfer_toolbar',
    name: 'options:transfer 定义可编辑组件工具栏（toolbar）',
    template: `<template>
  <iview-uieditor :options="options" :json="json" :theme="theme" />
</template>
<script>
  export default {
  data() {
    return {
      options: UERender.DefineOption({
        transfer:UERender.DefineTransfer({
        'demo-div': {
          type:'div',
          "editor": {
            text: 'Div 块级标签',
            order: 0,
            groupOrder:10,
            group:'Demo组件库/基础组件',
            icon: 'layui-icon layui-icon-template-1',
            container: true,
            toolbar({ render, service }) {
              return [
                {
                  title: "添加Text",
                  icon: "layui-icon layui-icon-addition",
                  click() {
                    service.addByComponent(
                      {
                        $isTmpl: true,
                        item: {
                          json:
                            '{"type":"uieditor-text","props":{"text":"test1222"}}',
                        },
                      },
                      render.editorId,
                      "in"
                    );
                  },
                },
              ];
            }
          }
        }
      }),
      }),
      theme: {},
      json: {
        type: "Card",
        props: {
          class: "mb-sm",
          "title": "Card 卡片",
        },
        children: [
          {
            type: "uieditor-text",
            props: {
              text: "文本内容",
            },
          },
        ],
      },
    };
  },
};
</script>`
  },
  {
    id: 'options_transfer_tabs',
    name: 'options:transfer 定义可编辑组件复杂容器（Tabs）',
    template: `<template>
  <iview-uieditor :options="options" :json="json" :theme="theme" />
</template>
<script>
  export default {
  data() {
    return {
      options: UERender.DefineOption({
        transfer: UERender.DefineTransfer({
          "demo-Tabs": {
            type: "Tabs",
            editor: {
              order: 6,
              groupOrder: 10,
              group: "Demo组件库/基础组件",
              text: "Tabs 标签页",
              icon: "ivu-icon ivu-icon-ios-folder-outline",
              container: true,
              controlLeft: false,
              json: {
                props: {
                  [UECanNotMoveInProps]: true,
                  [UECanNotMoveOutProps]: true,
                  [UECanNotRemoveChildProps]: true,
                  [UECanNotMoveChildProps]: true,
                },
              },
              toolbar({ render, service }) {
                return [
                  {
                    title: "添加",
                    icon: "layui-icon layui-icon-addition",
                    click() {
                      const name = "tab" + UEHelper.makeAutoId();
                      service.setRenderTemp(render.editorId, "tabs_cur", name);
                      service
                        .addByJson(
                          {
                            type: "demo-TabPane",
                            props: {
                              name: name,
                              label: \`Tab$\{_.size(render.children) + 1}\`,
                            },
                          },
                          render.editorId,
                          "in"
                        )
                        .then(function () {
                          service.setCurrent(render.editorId);
                        });
                    },
                  },
                ];
              },
              attrs: {
                type: {
                  order: 1,
                  type: "select",
                  datas: ["line", "card"],
                  effect: true,
                },
                animated: {
                  order: 2,
                  type: "boolean",
                  bind: true,
                },
                name: {
                  order: 3,
                  effect: true,
                },
                "on-click,on-tab-remove": { event: true, order: 30 },
              },
              transferAttr({ render, service, editing }) {
                if (!render.children || render.children.length == 0) {
                  render.children = [{ type: "demo-TabPane" }];
                }

                const props = render.props;
                if (editing) {
                  const id = render.editorId;
                  const temp = service.getRenderTemp(id, "tabs_cur");
                  props["value"] = temp;
                  props[":animated"] = "false";
                  props[
                    "@on-click"
                  ] = \`$service.setRenderTemp('$\{id}', 'tabs_cur', $event)\`;

                  const name = render.attrs?.name.value || "";
                  _.forEach(render.children, function (item) {
                    if (item.attrs) item.attrs.tab.value = name;
                    else if (item.props) item.props.tab = name;
                  });
                } else {
                  delete props["value"];
                  delete props[":animated"];
                  delete props["@on-click"];
                }
              },
            },
          },
          "demo-TabPane": {
            type: "TabPane",
            editor: {
              text: "TabPane：%label%",
              order: 7,
              showInTree: false,
              container: true,
              coping({ render, parent, service }) {
                const name = "tab" + UEHelper.makeAutoId();
                let pId = parent?.editorId;
                service.setRenderTemp(pId, "tabs_cur", name);
                render.props = _.assign({}, render.props, {
                  name,
                });
                return true;
              },
              attrs: {
                "label:Tab1,name:tab1,tab,icon": {
                  order: 1,
                  effect: true,
                },
                tab: { order: 10, effect: true, show: false },
              },
            },
          },
        }),
      }),
      theme: {},
      json: {
        type: "Card",
        props: {
          class: "mb-sm",
          title: "Card 卡片",
        },
        children: [
          {
            type: "uieditor-text",
            props: {
              text: "文本内容",
            },
          },
        ],
      },
    };
  },
};
</script>`
  }
];
